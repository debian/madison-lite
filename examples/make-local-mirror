#! /bin/sh
set -e

# This is an example of how to build a local mirror containing only the
# index files, not the packages themselves. If you put something similar to
# this in a cron job, then you can run madison-lite locally. Be sure to
# change the various hostnames to appropriate mirrors, set MIRROR to a
# suitable directory, and fill the mirror directory into your madison-lite
# configuration file.
#
# If you are reading this after the release of sarge, then you may need to
# change the lists of distributions and architectures.

HOST_MAIN=${HOST_MAIN:-'http://ftp.debian.org/debian'}
HOST_SECURITY=${HOST_SECURITY:-'http://security.debian.org/debian-security'}
HOST_UBUNTU=${HOST_UBUNTU:-'http://archive.ubuntu.com/ubuntu'}
HOST_UBUNTU_PORTS=${HOST_UBUNTU_PORTS:-'http://ports.ubuntu.com/ubuntu-ports'}
MIRROR=${MIRROR:-/tmp/mirror}

WGET_OPTS=${WGET_OPTS-'-q -N --passive-ftp'}

umask 002

mkdir -p "$MIRROR"
cd "$MIRROR"

mkdir -p dists
cd dists

# To mirror Ubuntu index files as well, run this script with:
#   ARCHIVES='main security ubuntu ubuntu-ports'
# set in the environment.
archives=${ARCHIVES:-'main security'}

for archive in $archives; do
    case $archive in
	main)
	    host="$HOST_MAIN"
	    suites='stable proposed-updates testing testing-proposed-updates unstable experimental'
	    components='main contrib non-free'
	    ;;
	security)
	    host="$HOST_SECURITY"
	    suites='stable/updates:stable-security testing-security'
	    components='main contrib non-free'
	    ;;
	ubuntu)
	    host="$HOST_UBUNTU"
	    suites='precise trusty xenial bionic cosmic disco eoan'
	    components='main restricted universe multiverse'
	    ;;
	ubuntu-ports)
	    host="$HOST_UBUNTU_PORTS"
	    suites='precise trusty xenial bionic cosmic disco eoan'
	    components='main restricted universe multiverse'
	    ;;
	*)
	    echo "Internal error: archive '$archive'?" >&2
	    exit 1
    esac

    for suite in $suites; do
	case $suite in
	    *:*)
		suitehere="${suite#*:}"
		suite="${suite%%:*}"
		;;
	    *)
		suitehere="$suite"
		;;
	esac

	mkdir -p "$suitehere"
	root=dists
	case $archive:$suite in
	    *:oldstable|*:stable|*:testing|*:unstable|*:experimental)
		arches='amd64 arm64 armel armhf i386 mips mips64el mipsel ppc64el s390x'
		;;
	    ubuntu:*)
		arches='amd64 i386'
		;;
	    ubuntu-ports:precise)
		arches='armel armhf powerpc'
		;;
	    ubuntu-ports:trusty)
		arches='arm64 armhf powerpc ppc64el'
		;;
	    ubuntu-ports:xenial)
		arches='arm64 armhf powerpc ppc64el s390x'
		;;
	    ubuntu-ports:bionic|ubuntu-ports:cosmic|ubuntu-ports:disco|ubuntu-ports:eoan)
		arches='arm64 armhf ppc64el s390x'
		;;
	esac
	case $archive:$suite in
	    ubuntu*:precise|ubuntu*:trusty)
		ext='.bz2'
		;;
	    *)
		ext='.xz'
		;;
	esac
	for component in $components; do
	    mkdir -p "$suitehere/$component"
	    for arch in $arches; do
		mkdir -p "$suitehere/$component/binary-$arch"
		wget $WGET_OPTS -O "$suitehere/$component/binary-$arch/Packages$ext" "$host/$root/$suite/$component/binary-$arch/Packages$ext"
	    done
	    mkdir -p "$suitehere/$component/source"
	    wget $WGET_OPTS -O "$suitehere/$component/source/Sources$ext" "$host/$root/$suite/$component/source/Sources$ext"
	done
    done
done

exit 0
